<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Partner
 *
 * @ORM\Table(name="partners")
 * @ORM\Entity
 */
class Partner
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=20, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="img_url", type="string", length=500, nullable=false)
     */
    private $imgUrl;

    /**
     * @var string|null
     *
     * @ORM\Column(name="description", type="string", length=10000, nullable=true)
     */
    private $description;

    /**
     * @var Collection
     *
     * @ORM\ManyToMany(targetEntity="Progetto", mappedBy="partner")
     */
    private $project;

    /**
     * @var string
     *
     * @ORM\Column(name="link", type="string", length=500, nullable=false)
     */
    private $link;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->project = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getImgUrl(): ?string
    {
        return $this->imgUrl;
    }

    public function setImgUrl(string $imgUrl): self
    {
        $this->imgUrl = $imgUrl;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return Collection|Progetto[]
     */
    public function getProject(): Collection
    {
        return $this->project;
    }

    public function addProject(Progetto $project): self
    {
        if (!$this->project->contains($project)) {
            $this->project[] = $project;
            $project->addPartner($this);
        }

        return $this;
    }

    public function removeProject(Progetto $project): self
    {
        if ($this->project->contains($project)) {
            $this->project->removeElement($project);
            $project->removePartner($this);
        }

        return $this;
    }

    public function getLink(): ?string
    {
        return $this->link;
    }

    public function setLink(string $link): self
    {
        $this->link = $link;

        return $this;
    }

    public function __toString()
    {
        return $this->name;
    }

}
