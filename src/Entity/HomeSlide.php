<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * HomeSlide
 *
 * @ORM\Table(name="home_slides")
 * @ORM\Entity
 */
class HomeSlide
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="img_url", type="string", length=500, nullable=false)
     */
    private $imgUrl;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getImgUrl(): ?string
    {
        return $this->imgUrl;
    }

    public function setImgUrl(string $imgUrl): self
    {
        $this->imgUrl = $imgUrl;

        return $this;
    }


}
