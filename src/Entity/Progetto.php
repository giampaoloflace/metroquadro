<?php

namespace App\Entity;

use App\Entity\Settore;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Progetto
 *
 * @ORM\Table(name="projects", indexes={@ORM\Index(name="sector", columns={"sector"})})
 * @ORM\Entity(repositoryClass="App\Repository\ProgettiRepository")
 */
class Progetto
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;
    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=80, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="content", type="string", length=10000, nullable=false)
     */
    private $content;

    /**
     * @var string
     *
     * @ORM\Column(name="image", type="string", length=500, nullable=false)
     */
    private $image;
    /**
     * @var string|null
     *
     * @ORM\Column(name="customer", type="string", length=80, nullable=true)
     */
    private $customer;
    /**
     * @var string
     *
     * @ORM\Column(name="place", type="string", length=500, nullable=false)
     */
    private $place;
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="date", nullable=false)
     */
    private $date;
    /**
     * @var Settore
     *
     * @ORM\ManyToOne(targetEntity="Settore")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="sector", referencedColumnName="id")
     * })
     */
    private $sector;
    /**
     * @var Collection
     *
     * @ORM\ManyToMany(targetEntity="Partner", inversedBy="project")
     * @ORM\JoinTable(name="projects_partners",
     *   joinColumns={
     *     @ORM\JoinColumn(name="project", referencedColumnName="id")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="partner", referencedColumnName="id")
     *   }
     * )
     */
    private $partner;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->partner = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;
        return $this;
    }

    public function getImage(): ?string
    {
        return $this->image;
    }

    public function setImage(string $image): self
    {
        $this->image = $image;
        return $this;
    }

    public function getCustomer(): ?string
    {
        return $this->customer;
    }

    public function setCustomer(?string $customer): self
    {
        $this->customer = $customer;
        return $this;
    }

    public function getPlace(): ?string
    {
        return $this->place;
    }

    public function setPlace(string $place): self
    {
        $this->place = $place;
        return $this;
    }

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function setContent(string $content): self
    {
        $this->content = $content;
        return $this;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;
        return $this;
    }

    public function getSector(): ?Settore
    {
        return $this->sector;
    }

    public function setSector(?Settore $sector): self
    {
        $this->sector = $sector;
        return $this;
    }

    /**
     * @return Collection|Partner[]
     */
    public function getPartner(): Collection
    {
        return $this->partner;
    }

    public function addPartner(Partner $partner): self
    {
        if (!$this->partner->contains($partner)) {
            $this->partner[] = $partner;
        }
        return $this;
    }

    public function removePartner(Partner $partner): self
    {
        if ($this->partner->contains($partner)) {
            $this->partner->removeElement($partner);
        }
        return $this;
    }

    public function __toString()
    {
        return $this->name;
    }


}
