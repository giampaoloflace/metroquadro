<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * OtherInfo
 *
 * @ORM\Table(name="other_info")
 * @ORM\Entity
 */
class OtherInfo
{
    /**
     * @var string
     *
     * @ORM\Column(name="chiave", type="string", length=50, nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $chiave;

    /**
     * @var string
     *
     * @ORM\Column(name="valore", type="string", length=10000, nullable=false)
     */
    private $valore;

    public function getChiave(): ?string
    {
        return $this->chiave;
    }

    public function getValore(): ?string
    {
        return $this->valore;
    }

    public function setValore(string $valore): self
    {
        $this->valore = $valore;

        return $this;
    }

    public function __toString()
    {
        return $this->valore;
    }

}
