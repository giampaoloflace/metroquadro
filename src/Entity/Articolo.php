<?php

namespace App\Entity;

use App\Entity\Categoria;
use DateTime;
use Doctrine\ORM\Mapping as ORM;

/**
 * Articolo
 *
 * @ORM\Table(name="articles", indexes={@ORM\Index(name="catId", columns={"catId"})})
 * @ORM\Entity(repositoryClass="App\Repository\NotizieRepository")
 */
class Articolo
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=80, nullable=false)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="image", type="string", length=500, nullable=true)
     */
    private $image;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="date", type="date", nullable=true)
     */
    private $date;

    /**
     * @var string
     *
     * @ORM\Column(name="content", type="string", length=10000, nullable=false)
     */
    private $content;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="is_news", type="boolean", nullable=false, options={"default"="1"})
     */
    private $isNews = '1';

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function setContent(string $content): self
    {
        $this->content = $content;

        return $this;
    }

    public function getIsNews(): ?bool
    {
        return $this->isNews;
    }

    public function setIsNews(?bool $isNews): self
    {
        $this->isNews = $isNews;

        return $this;
    }

    public function getImage(): ?string
    {
        return $this->image;
    }

    public function setImage(string $image): self
    {
        $this->image = $image;

        return $this;
    }

}
