<?php

namespace App\Controller;

use App\Entity\Progetto;
use App\Form\ProgettoType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin/progetti")
 */
class AdminProgettiController extends AbstractController
{
    /**
     * @Route("/", name="progetti_admin_index", methods="GET")
     */
    public function index(): Response
    {
        $progettos = $this->getDoctrine()
            ->getRepository(Progetto::class)
            ->findAll();

        return $this->render('admin/progetti/index.html.twig', ['progettos' => $progettos]);
    }

    /**
     * @Route("/new", name="progetti_admin_new", methods="GET|POST")
     * @param Request $request
     * @return Response
     */
    public function new(Request $request): Response
    {
        $progetto = new Progetto();
        $form = $this->createForm(ProgettoType::class, $progetto);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($progetto);
            $em->flush();

            return $this->redirectToRoute('progetti_admin_index');
        }

        return $this->render('admin/progetti/new.html.twig', [
            'progetto' => $progetto,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="progetti_admin_show", methods="GET")
     * @param Progetto $progetto
     * @return Response
     */
    public function show(Progetto $progetto): Response
    {
        return $this->render('admin/progetti/show.html.twig', ['progetto' => $progetto]);
    }

    /**
     * @Route("/{id}/edit", name="progetti_admin_edit", methods="GET|POST")
     * @param Request $request
     * @param Progetto $progetto
     * @return Response
     */
    public function edit(Request $request, Progetto $progetto): Response
    {
        $form = $this->createForm(ProgettoType::class, $progetto);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('progetti_admin_edit', ['id' => $progetto->getId()]);
        }

        return $this->render('admin/progetti/edit.html.twig', [
            'progetto' => $progetto,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="progetto_delete", methods="DELETE")
     * @param Request $request
     * @param Progetto $progetto
     * @return Response
     */
    public function delete(Request $request, Progetto $progetto): Response
    {
        if ($this->isCsrfTokenValid('delete'.$progetto->getId(), $request->request->get('_token'))) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($progetto);
            $em->flush();
        }

        return $this->redirectToRoute('progetti_admin_index');
    }
}
