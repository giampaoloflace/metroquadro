<?php

namespace App\Controller;

use App\Entity\Partner;
use App\Form\PartnerType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin/partners")
 */
class AdminPartnerController extends AbstractController
{
    /**
     * @Route("/", name="partner_admin_index", methods="GET")
     */
    public function index(): Response
    {
        $partners = $this->getDoctrine()
            ->getRepository(Partner::class)
            ->findAll();

        return $this->render('admin/partners/index.html.twig', ['partners' => $partners]);
    }

    /**
     * @Route("/new", name="partner_admin_new", methods="GET|POST")
     * @param Request $request
     * @return Response
     */
    public function new(Request $request): Response
    {
        $partner = new Partner();
        $form = $this->createForm(PartnerType::class, $partner);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($partner);
            $em->flush();

            return $this->redirectToRoute('partner_admin_index');
        }

        return $this->render('admin/partners/new.html.twig', [
            'partners' => $partner,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="partner_admin_show", methods="GET")
     * @param Partner $partner
     * @return Response
     */
    public function show(Partner $partner): Response
    {
        return $this->render('admin/partners/show.html.twig', ['partner' => $partner]);
    }

    /**
     * @Route("/{id}/edit", name="partner_admin_edit", methods="GET|POST")
     * @param Request $request
     * @param Partner $partner
     * @return Response
     */
    public function edit(Request $request, Partner $partner): Response
    {
        $form = $this->createForm(PartnerType::class, $partner);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('partner_admin_edit', ['id' => $partner->getId()]);
        }

        return $this->render('admin/partners/edit.html.twig', [
            'partner' => $partner,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="partner_delete", methods="DELETE")
     * @param Request $request
     * @param Partner $partner
     * @return Response
     */
    public function delete(Request $request, Partner $partner): Response
    {
        if ($this->isCsrfTokenValid('delete'.$partner->getId(), $request->request->get('_token'))) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($partner);
            $em->flush();
        }

        return $this->redirectToRoute('partner_admin_index');
    }
}
