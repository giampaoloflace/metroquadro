<?php

namespace App\Controller;

use App\Entity\Articolo;
use App\Entity\Servizio;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ServiziController extends AbstractController
{
    /**
     * @Route({"it": "/servizi", "en": "/services"}, name="servizi")
     * @param Request $request
     * @return Response
     */
    public function index(Request $request): Response
    {
        return $this->render('servizi/index.html.twig', [
            'locale' => strtoupper($request->getLocale()),
            'servizi' => $this->fetchServizi(),
        ]);
    }

    /**
     * @Route("/servizi/{page}", name="mostra_servizio")
     * @param Request $request
     * @param $page
     * @return Response
     */
    public function mostraServizio(Request $request, $page): Response
    {
        $articolo = $this->getDoctrine()
            ->getRepository(Articolo::class)
            ->findOneBy(['title' => str_replace('_', ' ', $page), 'isNews' => 0]);

        return $this->render('articoli/index.html.twig', [
            'locale' => $request->getLocale(),
            'articolo' => $articolo
        ]);
    }

    private function fetchServizi()
    {
        return $this->getDoctrine()
            ->getRepository(Servizio::class)
            ->findAll();
    }
}
