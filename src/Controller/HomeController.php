<?php

namespace App\Controller;

use App\Utility\Costanti;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    /**
     * @Route({"it": "/", "en": "/en"}, name="home")
     */
    public function index(): Response
    {
        return $this->render('home/index.html.twig', [
            'navArrows' => true,
            'mq_facebook' => Costanti::$FB_LINK,
            'mq_twitter' => Costanti::$TW_LINK,
            'mq_instagram' => Costanti::$IS_LINK
        ]);
    }
}
