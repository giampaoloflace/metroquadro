<?php

namespace App\Controller;

use App\Entity\Settore;
use App\Form\SettoreType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin/settori")
 */
class AdminSettoreController extends AbstractController
{
    /**
     * @Route("/", name="settore_admin_index", methods="GET")
     */
    public function index(): Response
    {
        $settores = $this->getDoctrine()
            ->getRepository(Settore::class)
            ->findAll();

        return $this->render('admin/settore/index.html.twig', ['settores' => $settores]);
    }

    /**
     * @Route("/new", name="settore_admin_new", methods="GET|POST")
     * @param Request $request
     * @return Response
     */
    public function new(Request $request): Response
    {
        $settore = new Settore();
        $form = $this->createForm(SettoreType::class, $settore);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($settore);
            $em->flush();

            return $this->redirectToRoute('settore_admin_index');
        }

        return $this->render('admin/settore/new.html.twig', [
            'settore' => $settore,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="settore_admin_show", methods="GET")
     * @param Settore $settore
     * @return Response
     */
    public function show(Settore $settore): Response
    {
        return $this->render('admin/settore/show.html.twig', ['settore' => $settore]);
    }

    /**
     * @Route("/{id}/edit", name="settore_admin_edit", methods="GET|POST")
     * @param Request $request
     * @param Settore $settore
     * @return Response
     */
    public function edit(Request $request, Settore $settore): Response
    {
        $form = $this->createForm(SettoreType::class, $settore);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('settore_admin_edit', ['id' => $settore->getId()]);
        }

        return $this->render('admin/settore/edit.html.twig', [
            'settore' => $settore,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="settore_delete", methods="DELETE")
     * @param Request $request
     * @param Settore $settore
     * @return Response
     */
    public function delete(Request $request, Settore $settore): Response
    {
        if ($this->isCsrfTokenValid('delete'.$settore->getId(), $request->request->get('_token'))) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($settore);
            $em->flush();
        }

        return $this->redirectToRoute('settore_admin_index');
    }
}
