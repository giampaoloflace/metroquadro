<?php

namespace App\Controller;

use App\Entity\OtherInfo;
use App\Entity\Staff;
use App\Utility\Costanti;
use Swift_Mailer;
use Swift_Message;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ContattiController extends AbstractController
{
    /**
     * @Route({"it": "/contatti", "en": "/contacts"}, name="contatti")
     * @param Request $request
     * @return Response
     */
    public function index(Request $request): Response
    {
        return $this->render('contatti/index.html.twig', [
            'locale' => strtoupper($request->getLocale()),
            'staff' => $this->fetchStaff(),
            'indicazioni_stradali' => $this->fetchIndicazioniStradali(),
            'email_status' => $request->query->get('email_status'),
            'mq_facebook' => Costanti::$FB_LINK,
            'mq_twitter' => Costanti::$TW_LINK,
            'mq_instagram' => Costanti::$IS_LINK
        ]);
    }

    /**
     * @Route("/contatti/email", name="email")
     * @param Request $request
     * @param Swift_Mailer $mailer
     * @return Response
     */
    public function email(Request $request, Swift_Mailer $mailer): Response
    {
        $status = ['email_status' => 'no'];
        $nome = $request->request->get('firstname');
        $cognome = $request->request->get('lastname');
        $email = $request->request->get('email');
        $oggetto = $request->request->get('oggetto') . '[' . $cognome . ' ' . $nome . ']';
        $messaggio = $request->request->get('message');

        if (\strlen($nome) > 2 && \strlen($cognome) > 2 && filter_var($email, FILTER_VALIDATE_EMAIL)
            && \strlen($oggetto) > 3 && \strlen($messaggio) > 4) {
            $msgToSend = (new Swift_Message($oggetto))
                ->setFrom($email)
                ->setTo(Costanti::$EMAIL_AZIENDALE)
                ->setBody($messaggio);
            if ($mailer->send($msgToSend)) {
                $status['email_status'] = 'ok';
            }
        }

        return $this->redirectToRoute('contatti', $status);
    }

    private function fetchStaff()
    {
        return $this->getDoctrine()
            ->getRepository(Staff::class)
            ->findAll();
    }

    private function fetchIndicazioniStradali(): string
    {
        return (string)$this->getDoctrine()
            ->getRepository(OtherInfo::class)
            ->find('INDICAZIONI_STRADALI');
    }

}
