<?php

namespace App\Controller;

use App\Entity\Progetto;
use App\Entity\Settore;
use App\Utility\PaginatorHelper;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ProgettiController extends AbstractController
{

    private static $RESULT_PER_PAGE = 6;

    /**
     * @Route({"it": "/progetti/{page}", "en": "/projects/{page}"}, name="progetti")
     * @param Request $request
     * @param int $page
     * @return Response
     */
    public function index(Request $request, $page = 1): Response
    {
        $progettiPaginator = $this->getProgettiPaginator($page);
        return $this->render('progetti/index.html.twig', [
            'locale' => strtoupper($request->getLocale()),
            'settori' => $this->fetchSettori(),
            'progetti' => $progettiPaginator,
            'actualPage' => $page,
            'maxPages' => ceil($progettiPaginator->count() / self::$RESULT_PER_PAGE)
        ]);
    }

    private function getProgettiPaginator($currentPage): Paginator
    {
        $query = $this->getDoctrine()->getRepository(Progetto::class)->getProgettiQuery();

        return PaginatorHelper::paginate($query,static::$RESULT_PER_PAGE, $currentPage);
    }

    private function fetchSettori()
    {
        return $this->getDoctrine()
            ->getRepository(Settore::class)
            ->findAll();
    }

}
