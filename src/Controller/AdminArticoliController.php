<?php

namespace App\Controller;

use App\Entity\Articolo;
use App\Form\ArticoloType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin/notizie")
 */
class AdminArticoliController extends AbstractController
{
    /**
     * @Route("/", name="notizie_admin_index", methods="GET")
     */
    public function index(): Response
    {
        $articolos = $this->getDoctrine()
            ->getRepository(Articolo::class)
            ->findBy(['isNews' => 1]);

        return $this->render('admin/notizie/index.html.twig', ['articolos' => $articolos]);
    }

    /**
     * @Route("/new", name="notizie_admin_new", methods="GET|POST")
     * @param Request $request
     * @return Response
     */
    public function new(Request $request): Response
    {
        $articolo = new Articolo();
        $form = $this->createForm(ArticoloType::class, $articolo);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($articolo);
            $em->flush();

            return $this->redirectToRoute('notizie_admin_index');
        }

        return $this->render('admin/notizie/new.html.twig', [
            'notizie' => $articolo,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="notizie_admin_show", methods="GET")
     * @param Articolo $articolo
     * @return Response
     */
    public function show(Articolo $articolo): Response
    {
        return $this->render('admin/notizie/show.html.twig', ['articolo' => $articolo]);
    }

    /**
     * @Route("/{id}/edit", name="notizie_admin_edit", methods="GET|POST")
     * @param Request $request
     * @param Articolo $articolo
     * @return Response
     */
    public function edit(Request $request, Articolo $articolo): Response
    {
        $form = $this->createForm(ArticoloType::class, $articolo);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('notizie_admin_edit', ['id' => $articolo->getId()]);
        }

        return $this->render('admin/notizie/edit.html.twig', [
            'articolo' => $articolo,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="notizie_admin_delete", methods="DELETE")
     * @param Request $request
     * @param Articolo $articolo
     * @return Response
     */
    public function delete(Request $request, Articolo $articolo): Response
    {
        if ($this->isCsrfTokenValid('delete'.$articolo->getId(), $request->request->get('_token'))) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($articolo);
            $em->flush();
        }

        return $this->redirectToRoute('notizie_admin_index');
    }
}
