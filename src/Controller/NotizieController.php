<?php

namespace App\Controller;

use App\Entity\Articolo;
use App\Utility\PaginatorHelper;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class NotizieController extends AbstractController
{
    private static $RESULT_PER_PAGE = 8;

    /**
     * @Route({"it": "/notizie/{page}", "en": "/news/{page}"}, name="notizie")
     * @param Request $request
     * @param int $page
     * @return Response
     */
    public function index(Request $request, $page = 1): Response
    {
        $notiziePaginator = $this->getNotiziePaginator();
        return $this->render('notizie/index.html.twig', [
            'locale' => strtoupper($request->getLocale()),
            'notizie' => $notiziePaginator,
            'actualPage' => $page,
            'maxPages' => ceil($notiziePaginator->count() / self::$RESULT_PER_PAGE)
        ]);
    }

    /**
     * @Route("/notizie/view/{id}", name="mostra_notizia",  requirements={"id"="\d+"})
     * @param Request $request
     * @param $id
     * @return Response
     */
    public function mostraNotizia(Request $request, $id): Response
    {
        return $this->render('articoli/index.html.twig', [
            'locale' => $request->getLocale(),
            'articolo' => $this->getDoctrine()->getRepository(Articolo::class)->find($id)
        ]);
    }

    private function getNotiziePaginator(): Paginator
    {
        $qb = $this->getDoctrine()
            ->getRepository(Articolo::class)
            ->getNotizieQueryBuilder();

        $query = $qb->andWhere('n.isNews = 1')->getQuery();

        return PaginatorHelper::paginate($query,static::$RESULT_PER_PAGE, $currentPage);
    }
}
