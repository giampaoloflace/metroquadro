<?php

namespace App\Controller;

use App\Entity\Partner;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class PartnersController extends AbstractController
{
    /**
     * @Route({"it": "/partners", "en": "/partners_en"}, name="partners")
     * @param Request $request
     * @return Response
     */
    public function index(Request $request): Response
    {
        return $this->render('partners/index.html.twig', [
            'locale' => strtoupper($request->getLocale()),
            'partners' => $this->fetchPartners(),
        ]);
    }

    private function fetchPartners()
    {
        return $this->getDoctrine()
            ->getRepository(Partner::class)
            ->findAll();
    }
}
