<?php

namespace App\Controller;

use App\Entity\OtherInfo;
use App\Entity\Staff;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ChiSiamoController extends AbstractController
{
    /**
     * @Route({"it": "/chi_siamo", "en": "/who_we_are"}, name="chi_siamo")
     * @param Request $request
     * @return Response
     */
    public function index(Request $request): Response
    {
        return $this->render('chi_siamo/index.html.twig', [
            'descrizione' => $this->fetchDescrizione(),
            'staff' => $this->fetchStaff(),
            'locale' => strtoupper($request->getLocale())
        ]);
    }

    private function fetchStaff()
    {
        return $this->getDoctrine()
            ->getRepository(Staff::class)
            ->findAll();
    }

    private function fetchDescrizione(): string
    {
        return (string) $this->getDoctrine()
            ->getRepository(OtherInfo::class)
            ->find('CHI_SIAMO');
    }
}
