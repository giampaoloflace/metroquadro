<?php

namespace App\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;

class NotizieRepository extends EntityRepository
{
    public function getNotizieQueryBuilder(): QueryBuilder
    {
        return $this->createQueryBuilder('n');
    }
}