<?php

namespace App\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;

class ProgettiRepository extends EntityRepository
{
    public function getProgettiQuery(): Query
    {
        return $this->createQueryBuilder('p')->getQuery();
    }
}