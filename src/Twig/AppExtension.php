<?php

namespace App\Twig;

use App\Utility\Costanti;
use Stichoza\GoogleTranslate\TranslateClient;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

class AppExtension extends AbstractExtension
{
    public function getFilters()
    {
        return array(
            new TwigFilter('res_asset', array($this, 'assetXDFilter')),
            new TwigFilter('ammazza_testo', array($this, 'ammazzaTestoFilter')),
            new TwigFilter('traduci', array($this, 'traduciFilter'))
        );
    }

    public function assetXDFilter($url): string
    {
        return Costanti::$URL . $url;
    }

    public function ammazzaTestoFilter($text, $lenght = 140): string
    {
        return substr($text, 0, $lenght) . '...';
    }

    public function traduciFilter($text) {
        try {
            $tr = new TranslateClient('it', 'en');
            /** @noinspection StaticInvocationViaThisInspection */
            return $tr->translate($text);
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }
}