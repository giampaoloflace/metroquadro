<?php

namespace App\Form;

use App\Entity\Articolo;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ArticoloType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options) : void
    {
        $builder
            ->add('title')
            ->add('image')
            ->add('date')
            ->add('content', TextareaType::class, ['attr' => array('class' => 'tinymce')])
            ->add('isNews', HiddenType::class, ['data' => 1]);
    }

    public function configureOptions(OptionsResolver $resolver) : void
    {
        $resolver->setDefaults([
            'data_class' => Articolo::class,
        ]);
    }
}
